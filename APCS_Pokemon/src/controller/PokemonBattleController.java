package controller;

import model.*;
import view.*;

import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class PokemonBattleController {
PokemonModel model;
    PokemonView view;
    PokemonController controller;

    Random random;

    PokemonBattleView battleWindow;

    Pokemon playerPokemon, enemyPokemon;

    int playerIndex;

    boolean isGettingMoves = false;

    public final static int FIGHT_OPTION = 0;
    public final static int POKEMON_OPTION = 1;
    public final static int BAG_OPTION = 2;
    public final static int RUN_OPTION = 3;


    public PokemonBattleController(int enemyIndex, PokemonModel model, PokemonView view, PokemonController controller){
        this.model = model;
        this.view = view;
        this.controller = controller;

        random = new Random();

        if(model.pokemonOwned.size() != 0){
            playerIndex = 0;

            playerPokemon = model.pokemonOwned.get(playerIndex);
            enemyPokemon = model.enemyPokemonOwned.get(enemyIndex);

            JOptionPane.showMessageDialog(view.window, "A wild " + model.enemyPokemonOwned.get(enemyIndex) + " appeared!", "Battle", JOptionPane.INFORMATION_MESSAGE, null);

            model.enemyPokemonOwned.get(enemyIndex).resetHP();

            battleWindow = new PokemonBattleView(controller, playerPokemon, enemyPokemon);

            battleWindow.addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent e) { }

                @Override
                public void keyPressed(KeyEvent e) {
                    if(e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_W){
                        battleWindow.moveCursor(PokemonController.UP);
                    }else if(e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_D){
                        battleWindow.moveCursor(PokemonController.RIGHT);
                    }else if(e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_S){
                        battleWindow.moveCursor(PokemonController.DOWN);
                    }else if(e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_A){
                        battleWindow.moveCursor(PokemonController.LEFT);
                    }else if(e.getKeyCode() == KeyEvent.VK_ENTER){
                        int choice = battleWindow.getChoice();
                        if(!isGettingMoves) {
                            if (choice == FIGHT_OPTION) {
                                askForMove();
                            } else if (choice == POKEMON_OPTION) {
                                battleWindow.displayYouHaveNoOtherPokemonMessage();
                            } else if (choice == BAG_OPTION) {
                                battleWindow.displayYouHaveNoItemsMessage();
                            } else if (choice == RUN_OPTION) {
                                endBattle();
                            }
                        }else{
                            moveSelected(choice);
                        }
                    }
                }

                @Override
                public void keyReleased(KeyEvent e) { }
            });

        }
    }

    public void moveSelected(int moveResponse){
        int damage = getDamageOfMove(playerPokemon.moves.get(moveResponse).moveName, playerPokemon);
        int accuracy = getAccuracyOfMove(playerPokemon.moves.get(moveResponse).moveName, playerPokemon);

        // Deal damage or miss
        if(random.nextInt(100) + 1 <= accuracy){
            battleWindow.displayUserDamageMessage(damage);
            enemyPokemon.loseHealth(damage);

            battleWindow.updatePokemonLabels();
        }else{
            battleWindow.displayUserMissedMessage();
        }
        // If enemy is defeated
        if(enemyPokemon.health <= 0){
            battleWindow.displayEnemyFaintMessage();

            battleWindow.displayWinMessage();

            int experience = (int)((enemyPokemon.level * 100.0) / 7.0);

            boolean levelUp = playerPokemon.gainExperience(experience);

            if(levelUp) battleWindow.displayLevelUpMessage();

            endBattle();

        }else{
            ArrayList<PokemonMove> enemyMoves = enemyPokemon.moves;

            PokemonMove theirMove = enemyMoves.get(random.nextInt(enemyMoves.size()));

            int theirDamage = getDamageOfMove(theirMove.moveName, enemyPokemon);
            int theirAccuracy = getAccuracyOfMove(theirMove.moveName, enemyPokemon);

            if (random.nextInt(100) + 1 <= theirAccuracy) {
                battleWindow.displayEnemyDamageMessage(theirMove.moveName, theirDamage);
                playerPokemon.loseHealth(theirDamage);

                if (playerPokemon.health <= 0) {
                    //show faint message and make the player go to the next pokemon
                    battleWindow.displayUserFaintMessage();
                    playerIndex++;
                    //if the pokemon that fainted was the last the player has, then end battle
                    if (model.pokemonOwned.size() - 1 < playerIndex) {
                        battleWindow.displayAllPokemonFaintedMessage();
                        controller.healAllPokemon();

                        endBattle();
                    }else{
                        battleWindow.displaySwitchingPokemonMessage(model.pokemonOwned.get(playerIndex));
                    }
                }

                battleWindow.updatePokemonLabels();

            }else{
                battleWindow.displayEnemyMissedMessage(theirMove.moveName);
            }
        }

        battleWindow.setLabelsToUserOptions();
        isGettingMoves = false;
    }

    public void askForMove(){
        String[] movesArray = new String[4];

        for(int i = 0; i < playerPokemon.moves.size(); i++){
            movesArray[i] = playerPokemon.moves.get(i).moveName;
        }

        battleWindow.setLabelsToMoves(movesArray);
        isGettingMoves = true;

    }

    public void endBattle(){
        battleWindow.close();
        controller.updatePokemonInfo();
    }

    // Returns the damage of the move based on the move and the pokemon level and attack
    public int getDamageOfMove(String move, Pokemon pokemon){
        int base = model.pokemonMoveHashtable.get(move).getDamageRating();

        return (int)(((2 * pokemon.level + 10) / 250.0) * (pokemon.attack / 40.0) * base + 2);
    }

    // Returns the accuracy of a move
    public int getAccuracyOfMove(String move, Pokemon pokemon){
        return model.pokemonMoveHashtable.get(move).getAccuracyRating();
    }
}
