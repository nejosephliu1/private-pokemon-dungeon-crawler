package controller;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import model.NPC;
import model.PokemonModel;
import model.Tile;
import view.PokemonView;

/*
 * The controller class of Pokemon. 
 * - Handles user interaction 
 * - Contains mouse, action, and key listeners
 */

public class PokemonController extends KeyAdapter{
	PokemonModel model;
	PokemonView view;
	
	// Constant for current map
	public final static int HOUSE = 1; //10x10
	public final static int PALLET_TOWN = 2; //10x10
	public final static int ROUTE_1 = 3; //30x30
	public final static int LAB = 4; //10x10
	public final static int POKECENTER = 5; //10x10
	public final static int POKEMART = 6; //10x10
	public final static int OUTSIDE_GYM = 7; //15x15
	public final static int GYM = 8; //20x20
	
	// Constants for directions
	public final static int FACING_UP = 0;
	public final static int FACING_RIGHT = 1;
	public final static int FACING_DOWN = 2;
	public final static int FACING_LEFT = 3;

	public final static int UP = 0;
	public final static int RIGHT = 1;
	public final static int DOWN = 2;
	public final static int LEFT = 3;

	// Constants during battle
	final static int BATTLE_FIGHT = 0;
	final static int BATTLE_ITEM = 1;
	final static int BATTLE_SWITCH = 2;
	final static int BATTLE_RUN = 3;

	// Width and Height of board
	final int visibleBoardWidth = 10;
	final int visibleBoardHeight = 10;

	//to show 1 dialog at a time for NPC chatting
	boolean NPCspeaking = false;
	
	final int chancesOfPokemonAppearance = 15; //Out of 100

	int mapHeight, mapWidth;
	int currentMap; // Current map

	int choice = -1;

	BufferedImage bulbasaurBack, charmanderBack, squirtleBack;
	BufferedImage pikachuFront, rattataFront;

	boolean canChoose = true;
	boolean stopAdvance = false;
    public PokemonController(int mapHeight, int mapWidth){

    	//currentMap = OUTSIDE_GYM; // Start off in a house
		currentMap = HOUSE;
		model = new PokemonModel(mapHeight, mapWidth, visibleBoardHeight, visibleBoardWidth);
		loadMap();
		// model.readLevelMap(new File("levels/test_tilemap_house"))
		view = new PokemonView(this);
		view.addKeyListener(this);
		
		updatePokemonInfo();

		loadPokemonSpriteImages();

		this.mapHeight = mapHeight;
		this.mapWidth = mapWidth;
	}

	public void loadPokemonSpriteImages(){
		try {
			bulbasaurBack = ImageIO.read(new File("images/pokemon_backs/bulbasaur_back.png"));
			charmanderBack = ImageIO.read(new File("images/pokemon_backs/charmander_back.png"));
			squirtleBack = ImageIO.read(new File("images/pokemon_backs/squirtle_back.png"));
			pikachuFront = ImageIO.read(new File("images/pokemon_fronts/pikachu_front.png"));
			rattataFront = ImageIO.read(new File("images/pokemon_fronts/rattata_front.png"));
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	public BufferedImage getPokemonImage(int pokemonID, boolean front){
		if(!front){
			if(pokemonID == model.SQUIRTLE_ID){
				return squirtleBack;
			}else if(pokemonID == model.CHARMANDER_ID){
				return charmanderBack;
			} else if(pokemonID == model.BULBASAUR_ID){
				return bulbasaurBack;
			}
		}else{
			if(pokemonID == model.PIKACHU_ID){
				return pikachuFront;
			}else if(pokemonID == model.RATTATA_ID){
				return rattataFront;
			}
		}


		return null;
	}

	public int getVisibleBoardWidth(){
		return visibleBoardWidth;
	}

	public int getVisibleBoardHeight(){
		return visibleBoardHeight;
	}

	public void loadMap(){
		model.offsetX = 0;
		model.offsetY = 0;
		if(currentMap == HOUSE){
			model.readLevelMap(new File("levels/test_tilemap_house"));
			model.setCharactersToDraw(HOUSE);
		}
		if(currentMap == PALLET_TOWN){
			model.readLevelMap(new File("levels/test_tilemap_pallet_town"));
			model.setCharactersToDraw(PALLET_TOWN);
		}
		if(currentMap == ROUTE_1){
			model.readLevelMap(new File("levels/test_tilemap_30x30"));
			model.setCharactersToDraw(ROUTE_1);
		} 
		if(currentMap == LAB){
			model.readLevelMap(new File("levels/test_tilemap_lab"));
			model.setCharactersToDraw(LAB);
		} 
		if(currentMap == POKEMART){
			model.readLevelMap(new File("levels/test_tilemap_pokemart"));
			model.setCharactersToDraw(POKEMART);
		} 
		if(currentMap == POKECENTER){
			model.readLevelMap(new File("levels/test_tilemap_pokecenter"));
			model.setCharactersToDraw(POKECENTER);
		}
		if(currentMap == OUTSIDE_GYM){
			model.readLevelMap(new File("levels/test_tilemap_outsidegym"));
			model.setCharactersToDraw(OUTSIDE_GYM);
		} 
		if(currentMap == GYM){
			model.readLevelMap(new File("levels/test_tilemap_gym"));
			model.setCharactersToDraw(GYM);
		}
	}
	
	public ArrayList<NPC> getNPCsToDraw(){
		return model.toDraw;
	}
	
	/*
    //This method is used in the driver after the View is created
    //Prevents the problem where controller needs view for constructor
    //param and view needs controller for constructor param
    public void setPokemonView(PokemonView view){
    	this.view = view;
    	view.addKeyListener(this);
    }*/
    
    private Tile[][] getTileArray(){
    	return model.visibleTileArray;
    }
    
    public Point getCharacterPosition(){
    	return model.charPos;
    }
    
    //direction he is facing, 1 = North, 2 = East, 3 = South, 4 - West
    public int getCharacterDirection(){
    	return model.charDir;
    }
    
    public Image getCurrentSprite(){
    	return model.currentSprite;
    }
    
    public void updateGameGraphics(){
    	view.updateGraphics();
    }


	// Returns the proper image at the index location in the tilArray
    // Passed into view class to be drawn
    public BufferedImage getImage(int row, int col){
    	BufferedImage image = null;
		try {
			//System.out.println("tile_images/"+getTileArray()[row][col].getImageName());
			image = ImageIO.read(new File("images/tile_images/"+getTileArray()[row][col].getImageName()));
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return image;
    }

    // Returns the proper image name at the index location in the tilArray
    // Passed into view class to be drawn
	public String getImageName(int row, int col){
		BufferedImage image = null;
		try {
			//System.out.println("tile_images/"+getTileArray()[row][col].getImageName());
			image = ImageIO.read(new File("images/tile_images/"+getTileArray()[row][col].getImageName()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "images/tile_images/"+getTileArray()[row][col].getImageName();
	}

	// Key events for movement
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_W){
			moveCharacter(FACING_UP, -1, 0);
		}else if(e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_D){
			moveCharacter(FACING_RIGHT, 0, 1);
		}else if(e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_S){
			moveCharacter(FACING_DOWN, 1, 0);
		}else if(e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_A){
			moveCharacter(FACING_LEFT, 0, -1);
		}
		else if(e.getKeyCode() == KeyEvent.VK_ENTER){
				ArrayList<NPC> npcs = model.charsOnLevel(currentMap);
				for(int i = 0; i < npcs.size(); i++){
					System.out.println(i);
					if(npcs.get(i).charIsFacing(model.charDir, model.charPos.x, model.charPos.y, model.offsetY, model.offsetX)){
						//npcs.get(i).showDialogue(view.window);
						Dimension viewDim = Toolkit.getDefaultToolkit().getScreenSize();
						//window size is 600
						int windowLeftX = viewDim.width/2 - 300;
						int windowTopY = viewDim.height/2 - 300;
						
						int gamePanelLeftX = windowLeftX + 100;
						int gamePanelTopY = windowTopY + 500;
						if(NPCspeaking == false){
							if(canChoose == false){
								//after choosing pokemon, Mom and Oak say different things
								String nameOfNPC = npcs.get(i).getName();
								if((nameOfNPC.equals("Professor Oak") || nameOfNPC.equals("Player's Mom"))&&!stopAdvance){
									npcs.get(i).advanceDialogue();
									stopAdvance = true;
								}
							}
							
							npcs.get(i).showDialogue(gamePanelLeftX, gamePanelTopY);
							npcs.get(i).isTalking = true;
							
							NPCspeaking = true;
						}
						else{
							if(npcs.get(i).isTalking == true)
								npcs.get(i).disposeDialogue();
								npcs.get(i).isTalking = false;
								NPCspeaking = false;
							}
						}
						
					}
				}
		}


	

	public void moveCharacter(int dir, int dy, int dx){
		if(model.charDir != dir) {
			model.setSprite(dir);
			view.updateGraphics();
		}else{
			int row = (int) model.charPos.getY();
			int col = (int) model.charPos.getX();

			if(model.validForWalking(row + dy, col + dx)){
				if(model.canShift(dir)){
					model.shiftVisibleBoard(dir);
				}else {
					model.charPos.setLocation(new Point(col + dx, row + dy));
				}

				view.updateGraphics();
			}
		}

		// If in tall grass, there is  a chance of encountering a Pokemon
		if(model.canPokemonAppear()){
			Random r = new Random();
			int randomInt = r.nextInt(100);
			if(randomInt < chancesOfPokemonAppearance){
				int whichPokemon = r.nextInt(model.enemyPokemonOwned.size());

				PokemonBattleController battle = new PokemonBattleController(whichPokemon, model, view, this);
				//pokemonBattle(whichPokemon);
			}
		}
		
		// If the character moves to an exit, move to next map, and change the position 
		// of the character
		if(currentMap == HOUSE){
			if(getTileArray()[(int)model.charPos.getY()][(int)model.charPos.getX()].getImageName().equals("exit_symbol.png")){
				currentMap = PALLET_TOWN;
				loadMap();
				model.charPos = new Point(8,8);
			}
			
			
		}else if(currentMap == PALLET_TOWN){
			if(model.charPos.getX()==8 && model.charPos.getY()==7){
				currentMap = HOUSE;
				loadMap();
				model.charPos = new Point(2,8);
			}else if(model.charPos.getX()==8 && model.charPos.getY()==3){
				currentMap = LAB;
				loadMap();
				model.charPos = new Point(5,8);
			}else if(model.charPos.getX()==4 && model.charPos.getY()==3){
				currentMap = POKEMART;
				loadMap();
				model.charPos = new Point(5,8);
			}else if((model.charPos.getX()==4 && model.charPos.getY()==9)||(model.charPos.getX()==5 && model.charPos.getY()==9)){
				currentMap = ROUTE_1;
				model.fullTileArray = new Tile[30][30];
				loadMap();
				model.charPos = new Point(0,0);
			
			}else if(model.charPos.getX()==2 && model.charPos.getY()==3){
				currentMap = POKECENTER;
				loadMap();
				model.charPos = new Point(5,8);
			}
			
			
		}else if(currentMap == LAB){
			if(getTileArray()[(int)model.charPos.getY()][(int)model.charPos.getX()].getImageName().equals("exit_symbol.png")){
				currentMap = PALLET_TOWN;
				loadMap();
				model.charPos = new Point(8,4);
			}else if(model.charPos.getX()==2 && model.charPos.getY()==4 && canChoose){
				canChoose = false;
				model.pokemonOwned.add(model.pokemonHashtable.get(model.CHARMANDER_ID));
				updatePokemonInfo();
				JOptionPane.showMessageDialog(view.window, "You chose Charmander!", "Charmander is awesome", JOptionPane.INFORMATION_MESSAGE, null);
			}
			else if(model.charPos.getX()==4 && model.charPos.getY()==4 && canChoose){
				canChoose = false;
				model.pokemonOwned.add(model.pokemonHashtable.get(model.SQUIRTLE_ID));
				updatePokemonInfo();
				JOptionPane.showMessageDialog(view.window, "You chose Squirtle!", "Squirtle is awesome", JOptionPane.INFORMATION_MESSAGE, null);
			}
			
			else if(model.charPos.getX()==6 && model.charPos.getY()==4 && canChoose){
				canChoose = false;
				model.pokemonOwned.add(model.pokemonHashtable.get(model.BULBASAUR_ID));
				updatePokemonInfo();
				JOptionPane.showMessageDialog(view.window, "You chose Bulbasaur!", "Bulbasaur is awesome", JOptionPane.INFORMATION_MESSAGE, null);
			}
			
			
			
			
		}else if(currentMap == POKEMART){
			if(getTileArray()[(int)model.charPos.getY()][(int)model.charPos.getX()].getImageName().equals("exit_symbol.png")){
				currentMap = PALLET_TOWN;
				loadMap();
				model.charPos = new Point(4,4);
			}
			
			
		}else if(currentMap == POKECENTER){
			if(getTileArray()[(int)model.charPos.getY()][(int)model.charPos.getX()].getImageName().equals("exit_symbol.png")){
				currentMap = PALLET_TOWN;
				loadMap();
				model.charPos = new Point(2,4);
			}
			
			
		}else if(currentMap == ROUTE_1){
			if(model.charPos.getX()==0 && model.charPos.getY()==0||model.charPos.getX()==1 && model.charPos.getY()==0){
				model.fullTileArray = new Tile[10][10];
				currentMap = PALLET_TOWN;
				loadMap();
				model.charPos = new Point(5,8);
			}else if(getTileArray()[(int)model.charPos.getY()][(int)model.charPos.getX()].getImageName().equals("exit_symbol.png")){
				currentMap = OUTSIDE_GYM;
				model.fullTileArray = new Tile[15][15];
				loadMap();
				model.charPos = new Point(1,1);
			}
			
			
			
		}else if(currentMap == OUTSIDE_GYM){
			System.out.println(model.charPos.getX());
			if(model.charPos.getX() + model.offsetX==14 && model.charPos.getY()+model.offsetY==10||model.charPos.getX()+model.offsetX==14 && model.charPos.getY()+model.offsetY==11){
				System.out.println("OUT");
				model.fullTileArray = new Tile[30][30];
				currentMap = ROUTE_1;
				loadMap();
				model.charPos = new Point(1,1);
			}if(getTileArray()[(int)model.charPos.getY()][(int)model.charPos.getX()].getImageName().equals("gymbottom.png")){
				System.out.println("Go into gym");
				model.charPos = new Point(2,2);
				model.fullTileArray = new Tile[20][20];
				currentMap = GYM;
				loadMap();
			}else if(getTileArray()[(int)model.charPos.getY()][(int)model.charPos.getX()].getImageName().equals("exit_symbol.png")){
				System.out.println("Go outside gym");
				currentMap = OUTSIDE_GYM;
				model.fullTileArray = new Tile[15][15];
				loadMap();
				model.charPos = new Point(14,10);
			}
			
			
			
		}else if(currentMap == GYM){
			if(getTileArray()[(int)model.charPos.getY()][(int)model.charPos.getX()].getImageName().equals("exit_symbol.png")){
				model.fullTileArray = new Tile[15][15];
				currentMap = OUTSIDE_GYM;
				loadMap();
				model.charPos = new Point(7,6);
			}
			
			
		}
		
		
	}

	public Point getOffset(){
		return new Point(model.offsetX, model.offsetY);
	}
	
	// Main Pokemon Battle || TEMPORARY -- WILL EVENTUALLY BE A GUI

	
	// Heals all the Pokemons in party
	public void healAllPokemon(){
		for(int i = 0; i < model.pokemonOwned.size(); i++){
			model.pokemonOwned.get(i).resetHP();
		}
	}
	
	// Update the JLabel to display your pokemon info
	public void updatePokemonInfo(){
		String text = "";
		int count = 1;
		for(int i=0;i<model.pokemonOwned.size();i++){
			if(count == 1){
				text += "<html>Lv." + model.pokemonOwned.get(i).level + " " + model.pokemonOwned.get(i).name +
						" HP:" + model.pokemonOwned.get(i).health + "/" + model.pokemonOwned.get(i).maxHealth + 
						" EXP:" + model.pokemonOwned.get(i).experience + "/100 <br>";
				count++;
			}else if(count == model.pokemonOwned.size()){
				text += " Lv." + model.pokemonOwned.get(i).level + " " + model.pokemonOwned.get(i).name +
						" HP:" + model.pokemonOwned.get(i).health + "/" + model.pokemonOwned.get(i).maxHealth + 
						" EXP:" + model.pokemonOwned.get(i).experience + "/100 </html>";
			}else{
				text += " Lv." + model.pokemonOwned.get(i).level + " " + model.pokemonOwned.get(i).name +
						" HP:" + model.pokemonOwned.get(i).health + "/" + model.pokemonOwned.get(i).maxHealth + 
						" EXP:" + model.pokemonOwned.get(i).experience + "/100 <br>";
			}
		}
		view.pokemonInfo.setText(text);
	}
	


	@Override
	public void keyReleased(KeyEvent e) {
		
		//if "escape" key is pressed, then toggle fullscreen mode
		if(e.getKeyCode() == KeyEvent.VK_F1){
			view.toggleFullscreen();
		}
		
	}

}