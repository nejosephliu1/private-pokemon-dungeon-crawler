package view;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.image.*;
import java.io.*;

import controller.PokemonBattleController;
import controller.PokemonController;
import model.Pokemon;

public class PokemonBattleView extends JFrame{
	
	BufferedImage backdrop;
	
	Pokemon playerPokemon, enemyPokemon;
	
	int windowWidth = 556;
	int windowHeight = 371;

	JLabel optionOne, optionTwo, optionThree, optionFour;

	int x1;
	int x2;
	int y1;
	int y2;

	PokemonController controller;

	int optionSelected = 0; // 1 - 4 scale

	MenuPanel menuPanel;

	JLabel playerPokemonLabel, enemyPokemonLabel;
	
	public PokemonBattleView(PokemonController controller, Pokemon playerPokemon, Pokemon enemyPokemon){

		this.controller = controller;
		this.playerPokemon = playerPokemon;
		this.enemyPokemon = enemyPokemon;

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
    	this.setSize(windowWidth, windowHeight);
		this.setLocation(dim.width/2-this.getWidth()/2, dim.height/2-this.getSize().height/2);
		
		this.setTitle("Pokemon Battle!");
		this.setLayout(null);
		
		try{
			backdrop = ImageIO.read(new File("charSprites/battle_template.png"));
		}catch(IOException e){
			e.printStackTrace();
		}



		BattlePanel battlePanel = new BattlePanel();
		battlePanel.setBounds(0, 0, windowWidth, windowHeight - 120);
		battlePanel.setLayout(null);

		playerPokemonLabel = new JLabel("Player: " + playerPokemon.name + " | Level " + playerPokemon.level + " | HP: " + playerPokemon.health);
		playerPokemonLabel.setBounds(25, 20, 250, 30);
		battlePanel.add(playerPokemonLabel);

		enemyPokemonLabel = new JLabel("Foe: " + enemyPokemon.name + " | Level " + enemyPokemon.level + " | HP: " + enemyPokemon.health);
		enemyPokemonLabel.setBounds(25, 40, 250, 30);
		battlePanel.add(enemyPokemonLabel);

		menuPanel = new MenuPanel();
		menuPanel.setLayout(null);
		menuPanel.setBounds(0, windowHeight - 120, windowWidth, 120);

		x1 = 3 * menuPanel.getWidth() / 7;
		x2 = 5 * menuPanel.getWidth() / 7;
		y1 = 0;
		y2 = 3 * menuPanel.getHeight() / 7;

		Font labelFont = new Font("Arial", Font.PLAIN, 21);

		optionOne = new JLabel("FIGHT");
		optionTwo = new JLabel("POKeMON");
		optionThree = new JLabel("BAG");
		optionFour = new JLabel("RUN");

		optionOne.setFont(labelFont);
		optionTwo.setFont(labelFont);
		optionThree.setFont(labelFont);
		optionFour.setFont(labelFont);

		optionOne.setBounds(x1, y1, 130, 50);
		optionTwo.setBounds(x1, y2, 130, 50);
		optionThree.setBounds(x2, y1, 130, 50);
		optionFour.setBounds(x2, y2, 130, 50);

		menuPanel.add(optionOne);
		menuPanel.add(optionTwo);
		menuPanel.add(optionThree);
		menuPanel.add(optionFour);

		this.add(menuPanel);
		this.add(battlePanel);
		this.setResizable(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setVisible(true);
	}
	
	public void displayUserDamageMessage(int damage){
		JOptionPane.showMessageDialog(this, "You dealt " + damage + " damage!", "Battle", JOptionPane.INFORMATION_MESSAGE, null);
	}
	
	public void displayUserMissedMessage(){
		JOptionPane.showMessageDialog(this, "You missed!", "Battle", JOptionPane.INFORMATION_MESSAGE, null);
	}
	
	public void displayEnemyDamageMessage(String moveName, int theirDamage){
		JOptionPane.showMessageDialog(this, enemyPokemon.name + " used " + moveName + ", dealing " + theirDamage + " damage!" , "Battle", JOptionPane.INFORMATION_MESSAGE, null);   
	}
	
	public void displayEnemyMissedMessage(String moveName){
		JOptionPane.showMessageDialog(this, enemyPokemon.name + " used " + moveName + " but missed!", "Battle", JOptionPane.INFORMATION_MESSAGE, null);
	}
	
	public void displayUserFaintMessage(){
		JOptionPane.showMessageDialog(this, playerPokemon.name + " has fainted! ", "Battle", JOptionPane.INFORMATION_MESSAGE, null);
	}
	
	public void displayEnemyFaintMessage(){
		JOptionPane.showMessageDialog(this, "Enemy " + enemyPokemon.name + " has fainted!", "Battle", JOptionPane.INFORMATION_MESSAGE, null);
	}
	
	public void displayAllPokemonFaintedMessage(){
		JOptionPane.showMessageDialog(this, "All your pokemon fainted! You rush to the Pokemon Center to heal them.", "Defeat", JOptionPane.INFORMATION_MESSAGE, null);
	}
	
	public void displayLevelUpMessage(){
		JOptionPane.showMessageDialog(this, "Your " + playerPokemon.name + " has leveled up!",
				"Battle", JOptionPane.INFORMATION_MESSAGE, null);
	}
	
	public void displayWinMessage(){
		JOptionPane.showMessageDialog(this, "You won the pokemon battle!", "Victory", JOptionPane.INFORMATION_MESSAGE, null);
	}
	
	public void close(){
		this.dispose();
	}

	public void updatePokemonLabels(){
		int playerHP = playerPokemon.health;
		int enemyHP = enemyPokemon.health;

		if(playerHP < 0) playerHP = 0;
		if(enemyHP < 0) enemyHP = 0;

		playerPokemonLabel.setText("Player: " + playerPokemon.name + " | Level " + playerPokemon.level + " | HP: " + playerHP);
		enemyPokemonLabel.setText("Foe: " + enemyPokemon.name + " | Level " + enemyPokemon.level + " | HP: " + enemyHP);
	}

	public int getChoice(){
		return optionSelected;
	}

	public void setLabelsToUserOptions(){
		optionSelected = 0;

		optionOne.setText("FIGHT");
		optionTwo.setText("POKeMON");
		optionThree.setText("BAG");
		optionFour.setText("RUN");

		menuPanel.repaint();
	}

	public void setLabelsToMoves(String[] movesArray){
		optionSelected = 0;

		optionOne.setText(movesArray[0]);
		optionTwo.setText(movesArray[1]);
		optionThree.setText(movesArray[2]);
		optionFour.setText(movesArray[3]);

		menuPanel.repaint();
	}

	public void displaySwitchingPokemonMessage(Pokemon pokemon){
		JOptionPane.showMessageDialog(this, "Go, " + pokemon + ", I choose you!", "Switching Pokemon", JOptionPane.INFORMATION_MESSAGE, null);
	}

	public void displayYouHaveNoOtherPokemonMessage(){
		JOptionPane.showMessageDialog(this, "You have no other Pokemon.", "Pokemon", JOptionPane.INFORMATION_MESSAGE);
	}

	public void displayYouHaveNoItemsMessage(){
		JOptionPane.showMessageDialog(this, "You have no items.", "Bag", JOptionPane.INFORMATION_MESSAGE);
	}

	public void moveCursor(int dir){
		if(dir == PokemonController.UP || dir == PokemonController.DOWN){
			if(optionSelected == 0) optionSelected = 1;
			else if(optionSelected == 1) optionSelected = 0;
			else if(optionSelected == 2) optionSelected = 3;
			else if(optionSelected == 3) optionSelected = 2;
		}else if(dir == PokemonController.RIGHT || dir == PokemonController.LEFT){
			if(optionSelected == 0) optionSelected = 2;
			else if(optionSelected == 2) optionSelected = 0;
			else if(optionSelected == 1) optionSelected = 3;
			else if(optionSelected == 3) optionSelected = 1;
		}
		menuPanel.repaint();

	}
	
	class BattlePanel extends JPanel{
		@Override
		public void paintComponent(Graphics g){
			g.drawImage(backdrop, 0, 0, windowWidth, windowHeight, null);

			g.drawImage(controller.getPokemonImage(playerPokemon.pokemonID, false), 90, 150, 100, 100, null);

			g.drawImage(controller.getPokemonImage(enemyPokemon.pokemonID, true), 365, 60, 100, 100, null);

		}

	}

	class MenuPanel extends JPanel{
		@Override
		public void paintComponent(Graphics g){
			g.setColor(Color.lightGray);
			
			g.fillRect(0, 0, getWidth(), getHeight());
			
			g.setColor(Color.black);
			int x = 0, y = 0;

			if(optionSelected == PokemonBattleController.FIGHT_OPTION){
				x = x1;
				y = y1;
			}else if(optionSelected == PokemonBattleController.POKEMON_OPTION){
				x = x1;
				y = y2;
			}else if(optionSelected == PokemonBattleController.BAG_OPTION){
				x = x2;
				y = y1;
			}else if(optionSelected == PokemonBattleController.RUN_OPTION){
				x = x2;
				y = y2;
			}

			drawTriangle(x - 18, y + 15, g);
		}

		public void drawTriangle(int x, int y, Graphics g){
			int c = 0;
			for(int i = 20; i >= 10; i--){
				g.drawLine(x, y + c, x, y + i);
				x++;
				c++;
			}
		}
	}
	
}
