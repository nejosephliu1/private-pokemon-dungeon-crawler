package view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PokemonDialogueView extends JFrame {
	BufferedImage textBackground = null;
	JLabel words;
	String text;
	String textWrapped;
	int width;
	int height;
	final String html1 = "<html><body style='width: ";
    final String html2 = "px'>";

	public PokemonDialogueView(String textToDisplay, String name, int x, int y) {
		width = 400;
		height = 100;
		text = "     " + name + ": " + textToDisplay;
		//for word wrapping using html in JLabel:
		words = new JLabel(html1 + "300" + html2 + text);
		words.setBounds(x, y, width, height);
		
		this.setBounds(x, y, width, height);
		this.setResizable(false);
		this.setUndecorated(true);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		try {
			textBackground = ImageIO.read(new File("images/NPCdialogueBox.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		textPanel NPCchat = new textPanel();
		NPCchat.add(words);
		this.add(NPCchat);
		this.setVisible(true);
	}

	class textPanel extends JPanel {
		@Override
		public void paintComponent(Graphics g) {
			g.drawImage(textBackground, 0, 0, width, height, null);
		}
	}

}
