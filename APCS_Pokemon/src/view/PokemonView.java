package view;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.*;

import controller.PokemonController;
import model.NPC;

public class PokemonView {
	public JFrame window;
	public GamePanel gamePanel;
	public JLabel pokemonInfo;
	PokemonController controller;

	Dimension dim;
	boolean fullscreen = false;


	
    public PokemonView(PokemonController controller){
    	// Sets the controller
    	this.controller = controller;
    	
    	// Create JFrame
    	window = new JFrame("Pokemon Dungeon Crawler");
    	
    	dim = Toolkit.getDefaultToolkit().getScreenSize();
    	window.setSize(600, 600);
		window.setLocation(dim.width/2-window.getWidth()/2, dim.height/2-window.getSize().height/2);
		
    	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	
    	window.setResizable(false);

    	window.setLayout(null);
    	
    	// Create the main Game Panel
    	gamePanel = new GamePanel();
    	gamePanel.setBounds(100, 100, 400, 400);
    	
    	// Create JLabel with hp and level of Pokemon
    	pokemonInfo = new JLabel();
    	pokemonInfo.setBackground(Color.red);
    	pokemonInfo.setBounds(100, 0, 500, 100);
  
    	// Adds elements to the GUI
    	window.add(gamePanel);
    	window.add(pokemonInfo);

    	// Set window to visible
    	window.setVisible(true);
    	
    	//Show controls in the beginning
    	JOptionPane help1 = new JOptionPane();
		help1.showMessageDialog(window, "Welcome to the the world of Pokemon!", "Welcome", JOptionPane.INFORMATION_MESSAGE, null);
		JOptionPane help2 = new JOptionPane();
		help2.showMessageDialog(window, "Use the W, A, S, and D keys to navigate this world.", "Controls Part 1", JOptionPane.INFORMATION_MESSAGE, null);	
		JOptionPane help3 = new JOptionPane();
		help3.showMessageDialog(window, "Use ENTER to interact with other people! Press ENTER a second time to finish talking with them.", "Controls Part 2", JOptionPane.INFORMATION_MESSAGE, null);	
		
    }
    
    
    public void updateGraphics(){
    	gamePanel.repaint();
    }
    /*
    public void drawNPCs(Graphics g, int tileWidth, int tileHeight){
    	ArrayList<NPC> drawings = controller.getNPCsToDraw();
    	for(int i = 0; i < drawings.size(); i++){
    		g.drawImage(drawings.get(i).getImage(), (int) drawings.get(i).getLocation().x * tileWidth,
					(int) drawings.get(i).getLocation().y * (tileHeight), tileWidth, tileHeight, null);
    		System.out.println("Test");
    	}
    }*/
    
    public void toggleFullscreen(){
    	//couldn't get the window.setUndecorated() to work (makes it truly full screen with no borders)
    	if(fullscreen == false){
    		window.dispose();
    		window = new JFrame("Pokemon Dungeon Crawler");
    		window.addKeyListener(controller);
    		
    		window.setSize(dim);
    		window.setLocation(dim.width/2-window.getSize().width/2, dim.height/2-window.getSize().height/2);
    		
    		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setResizable(false);
        	window.setLayout(null);
    		
    		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
    		gamePanel.setBounds(window.getWidth()/2 - 200, window.getHeight()/2 - 200, 400, 400);
    		window.add(gamePanel);
    		window.setVisible(true);
    		window.repaint();
    		fullscreen = true;
    	}
    	else if(fullscreen == true){
    		window.dispose();
    		window = new JFrame("Pokemon Dungeon Crawler");
    		window.addKeyListener(controller);
    		window.setSize(600, 600);
    		window.setLocation(dim.width/2-window.getWidth()/2, dim.height/2-window.getSize().height/2);
    		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setResizable(false);
        	window.setLayout(null);
    		gamePanel.setBounds(100, 100, 400, 400);
    		window.add(gamePanel);
    		window.setVisible(true);
    		window.repaint();
    		fullscreen = false;
    	}
    }
    
    
    public void addActionListener(ActionListener listener){
    	
    }
    
	public void addMouseListener(MouseListener listener){
	    	
	}

	public void addKeyListener(KeyListener listener){
		window.addKeyListener(listener);
	}
	
	// GamePanel of the GUI
	class GamePanel extends JPanel{
		
		// Asks controller for the image at a given location and then draws the image
		public void paintComponent(Graphics g){
			g.setColor(Color.white);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());

			g.setColor(Color.lightGray);
			//map

			int tileWidth = this.getWidth() / controller.getVisibleBoardWidth();
			int tileHeight = this.getHeight() / controller.getVisibleBoardHeight();

			for (int x = 0; x < this.getWidth(); x += tileWidth){
				for (int y = 0; y < this.getHeight(); y += tileHeight){
					if(y / tileHeight >= controller.getVisibleBoardHeight() || x / tileWidth >= controller.getVisibleBoardWidth()){

					}else{
						BufferedImage image = controller.getImage(y / tileHeight, x / tileWidth);

						g.drawImage(image, x, y, tileWidth, tileHeight, null);
					}

				}
			}
			//draw the player
			Image characterSprite = controller.getCurrentSprite();
			
			g.drawImage(characterSprite, (int) controller.getCharacterPosition().getX() * tileWidth,
					(int) controller.getCharacterPosition().getY() * (tileHeight), tileWidth, tileHeight, null);
			//System.out.println("C " + controller.getCharacterPosition().getY() + ", R: " + controller.getCharacterPosition().getX());
			
			//draw the NPCs
			ArrayList<NPC> drawings = controller.getNPCsToDraw();
			
	    	for(int i = 0; i < drawings.size(); i++){
	    		//scrolling for NPCs
	    		int charCol = (int) drawings.get(i).getLocation().getX() - (int)controller.getOffset().getX();
	    		int charRow = (int) drawings.get(i).getLocation().getY() - (int)controller.getOffset().getY();
	    		
	    		g.drawImage(drawings.get(i).getImage(), charCol * tileWidth,
						charRow * (tileHeight), tileWidth, tileHeight, null);
	    	}
			
		}
		
	}

}
