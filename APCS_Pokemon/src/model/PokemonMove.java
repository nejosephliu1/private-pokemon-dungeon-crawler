package model;

// Attributes for a Pokemon move

// moveName is the name of the move
// moveId is the ID for the move to be stored
// damageRating is how much the move damages by (it is affected by the Pokemon's level)
// accuracyRating is the change of the move hitting the enemy

// List of ID's: http://bulbapedia.bulbagarden.net/wiki/List_of_moves
public class PokemonMove{
	public String moveName;
	public int moveID;

	public int damageRating;
	public int accuracyRating;

    public PokemonMove(String name, int ID, int damageRating, int accuracyRating){
    	this.moveName = name;
    	this.moveID = ID;
		this.damageRating = damageRating;
		this.accuracyRating = accuracyRating;
    }

	public int getDamageRating(){
		return damageRating;
	}

	public int getAccuracyRating(){
		return accuracyRating;
	}


}
