package model;
/*
 * Pokemon Game Model Class
 * -Keeps track of the Pokemons the user has
 * -Keeps track of the Items the user has
 * 
 */

import controller.PokemonController;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

import javax.imageio.ImageIO;

public class PokemonModel {

    public static Hashtable<Integer, Pokemon> pokemonHashtable = new Hashtable<Integer, Pokemon>(); //Used to retrieve Pokemon objects

    public ArrayList<Pokemon> pokemonOwned = new ArrayList<Pokemon>(); //ArrayList will contain the Pokemons
    
    public ArrayList<Pokemon> enemyPokemonOwned = new ArrayList<Pokemon>(); //ArrayList will contain the Pokemons

    public Hashtable<String, PokemonMove> pokemonMoveHashtable = new Hashtable<String, PokemonMove>();

    int money;
    public Tile[][] fullTileArray;
    public Tile[][] visibleTileArray;
    public Point charPos; //position of character: x is the COL and y is the ROW number
    					  //refers to row/col in the fullTileArray
    public int charDir; //direction character is facing, 0 = North, 1 = East, 2 = South, 3 = West
    public Image[] playerSprites;
    public Image currentSprite; //the image the character is currently
    //all the NPCs in the game
    public ArrayList<NPC> characters = new ArrayList<NPC>();
    //the NPCs to draw (those that are on the current map)
    public ArrayList<NPC> toDraw = new ArrayList<NPC>();
    
    int visibleBoardHeight, visibleBoardWidth;

    public int offsetY = 0;
    public int offsetX = 0;
    
    public final int PIKACHU_ID = 25;
    public final int RATTATA_ID = 19;
    public final int CHARMANDER_ID = 4;
    public final int BULBASAUR_ID = 1;
    public final int SQUIRTLE_ID = 7;
    
    public PokemonModel(int mapHeight, int mapWidth, int visibleBoardHeight, int visibleBoardWidth){
    	charPos = new Point(4, 6); //character starts at top left for the test map
    	charDir = PokemonController.FACING_DOWN; //character is facing south
    	
        initMovesHashtable();

        initializeAllPokemon();

        fullTileArray = new Tile[mapHeight][mapWidth];
        visibleTileArray = new Tile[visibleBoardHeight][visibleBoardWidth];
        playerSprites = new Image[4]; //north east south west images to be put here
        fillCharSprites();
        currentSprite = playerSprites[PokemonController.FACING_DOWN]; //player is facing south at first
        
        enemyPokemonOwned.add(pokemonHashtable.get(PIKACHU_ID));
        enemyPokemonOwned.add(pokemonHashtable.get(RATTATA_ID));
        
        addAllNPCs();

        this.visibleBoardHeight = visibleBoardHeight;
        this.visibleBoardWidth = visibleBoardWidth;
    }

    public void initializeAllPokemon(){
    	//Starter Pokemon
        initPokemon("Charmander", 4, 39, 52, "Tackle", "Ember", "Flamethrower", "Fire Blast");
        initPokemon("Squirtle", 7, 44, 48, "Tackle", "Water Gun", "Hydro Pump", "Water Pulse");
        initPokemon("Bulbasaur", 1, 45, 49, "Tackle", "Razor Leaf", "Solar Beam", "Vine Whip");
        
        //Wild Pokemon
        initPokemon("Rattata", 19, 30, 56, "Tackle", "Quick Attack", "Hyper Fang", "Double-Edge");
        initPokemon("Pikachu", 25, 35, 55, "Tackle", "Thunder", "Iron Tail", "Thunder Shock");
        //initPokemon("Pidgey", 16, 40, 45, move1, move2, move3, move4);
    }

    public void addAllNPCs(){
    	//Pokemon Center
    	NPC nurse = new NPC(5, 2, 2, 5);
    	
    	//LAB:
    	NPC professorOak = new NPC(5, 5, 2, 4);
    	
    	//Player's house:
    	NPC mom = new NPC(5, 5, 2, 1);
    	
    	//Route 1
    	NPC trainer1 = new NPC(5, 10, 2, 3);
    	
    	//Sprites
    	Image nurseSouth = null;
    	Image oakSouth = null;
    	Image momSouth = null;
    	Image trainer1South = null;
    	
		try {
			oakSouth = ImageIO.read(new File("charSprites/profOakSouth.png"));
			momSouth = ImageIO.read(new File("charSprites/momSouth.png"));
			trainer1South = ImageIO.read(new File("charSprites/trainer1South.png"));
			nurseSouth = ImageIO.read(new File("charSprites/nurseSouth.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		nurse.setSprite(nurseSouth, 2);
    	professorOak.setSprite(oakSouth, 2);
    	mom.setSprite(momSouth, 2);
    	trainer1.setSprite(trainer1South, 2);
    	
    	nurse.name = "Nurse Joy";
    	professorOak.name = "Professor Oak";
    	mom.name = "Player's Mom";
    	trainer1.name = "Pokemon Trainer";
    	
    	nurse.addDialog("I'll heal your Pokemon for you... There! All set!");
    	professorOak.addDialog("Do you see those balls behind me? They are called PokeBalls. They hold pokemon inside! You may have one for your first Pokemon! Charmander is on the left, Squirtle is in the middle, and Bulbasaur is on the right.");
    	professorOak.addDialog("Ah, good choice! Now, as a trainer, you can help me collect data by finding and capturing the Pokemon you encounter on your journey! Good luck to your adventures as a Pokemon Trainer!");
    	mom.addDialog("Ash! Professor Oak said he had something to show you. You'd better get going then!");
    	mom.addDialog("Oh! I'm glad you have your Pokemon now. You are free to pursue your dreams as a Pokemon Trainer... Just remember to visit often!");
    	trainer1.addDialog("Hey, are you looking for Pokemon too? They often appear in tall grass and caves.");
    	characters.add(nurse);
    	characters.add(professorOak);
    	characters.add(mom);
    	characters.add(trainer1);
    }
    
    public void setCharactersToDraw(int level){
    	toDraw = charsOnLevel(level);
    }
    
    public ArrayList<NPC> charsOnLevel(int level){
    	ArrayList<NPC> npcsFound = new ArrayList<NPC>();
    	for(int i = 0; i < characters.size(); i++){
    		if(characters.get(i).getMap() == level) npcsFound.add(characters.get(i));
    	}
    	return npcsFound;
    }

    public void initPokemon(String name, int pokemonID, int maxHealth, int attack, String move1, String move2, String move3, String move4){
        Pokemon newPokemon = new Pokemon(name, pokemonID, maxHealth, attack);
        newPokemon.learnMove(pokemonMoveHashtable.get(move1));
        newPokemon.learnMove(pokemonMoveHashtable.get(move2));
        newPokemon.learnMove(pokemonMoveHashtable.get(move3));
        newPokemon.learnMove(pokemonMoveHashtable.get(move4));

        pokemonHashtable.put(pokemonID, newPokemon);
    }


    public void initMovesHashtable(){
        //Link: http://bulbapedia.bulbagarden.net/wiki/List_of_moves
        //                       "MOVE NAME",       new PokemonMove("MOVE NAME"       ID #    DAMAGE  ACCURACY));
        pokemonMoveHashtable.put("Tackle",          new PokemonMove("Tackle",         33,     50,     100));
        pokemonMoveHashtable.put("Thunder",         new PokemonMove("Thunder",        87,     110,    70));
        pokemonMoveHashtable.put("Iron Tail",       new PokemonMove("Iron Tail",      231,    100,    75));
        pokemonMoveHashtable.put("Thunder Shock",   new PokemonMove("Thunder Shock",  84,     110,    100));
        pokemonMoveHashtable.put("Ember",           new PokemonMove("Ember",          52,     40,     100));
        pokemonMoveHashtable.put("Flamethrower",    new PokemonMove("Flamethrower",   53,     90,     90));
        pokemonMoveHashtable.put("Fire Blast",      new PokemonMove("Fire Blast",     126,    110,    85));
        pokemonMoveHashtable.put("Quick Attack",    new PokemonMove("Quick Attack",   98,     40,     100));
        pokemonMoveHashtable.put("Hyper Fang",      new PokemonMove("Hyper Fang",     158,    80,     90));
        pokemonMoveHashtable.put("Double-Edge",     new PokemonMove("Double-Edge",    38,     120,    100));
        pokemonMoveHashtable.put("Water Gun",       new PokemonMove("Water Gun",      55,     40,     100));
        pokemonMoveHashtable.put("Hydro Pump",      new PokemonMove("Hydro Pump",     56,     110,    80));
        pokemonMoveHashtable.put("Water Pulse",     new PokemonMove("Water Pulse",    352,    60,     100));
        pokemonMoveHashtable.put("Solar Beam",      new PokemonMove("Solar Beam",     76,     120,    100));
        pokemonMoveHashtable.put("Razor Leaf",      new PokemonMove("Razor Leaf",     75,     55,     95));
        pokemonMoveHashtable.put("Vine Whip",       new PokemonMove("Vine Whip",      22,     45,     100));
        
    }
    
    public void setSprite(int direction){
    	//0 north, 1 east, 2 south, 3 west
    	currentSprite = playerSprites[direction];
    	charDir = direction;
    }

    //fills playerSprites array with the images needed
    public void fillCharSprites(){
    	BufferedImage facingNorth = null;
    	BufferedImage facingEast = null;
    	BufferedImage facingSouth = null;
    	BufferedImage facingWest = null;
    	
    	try {
    		facingNorth = ImageIO.read(new File("charSprites/ashSprite_north.png"));
    		facingEast = ImageIO.read(new File("charSprites/ashSprite_east.png"));
    		facingSouth = ImageIO.read(new File("charSprites/ashSprite_south.png"));
    		facingWest = ImageIO.read(new File("charSprites/ashSprite_west.png"));
    	} catch (IOException e) {
            e.printStackTrace();
    	}
    	playerSprites[PokemonController.FACING_UP] = facingNorth;
    	playerSprites[PokemonController.FACING_RIGHT] = facingEast;
    	playerSprites[PokemonController.FACING_DOWN] = facingSouth;
    	playerSprites[PokemonController.FACING_LEFT] = facingWest;
    }
    
    // Reads a file and loads the tiles into the fullTileArray
    // Tile is a class that keeps track of the name of the image and the boolean values associated with that particular tile
    // Spawn loc variable takes 1-top right, 2-bottom right
    public void readLevelMap(File f){
        Scanner sc = null;
        try{
            sc = new Scanner(f);
        }catch(IOException e){
            e.printStackTrace();
        }

        int row = 0;
        int col = 0;

        while(sc.hasNext()){
            String imgName = sc.next().substring(1);

            String walkableString = sc.next();
            boolean walkable = Boolean.parseBoolean(walkableString.substring(0, walkableString.length() - 1));

            //System.out.println("Image Name: " + imgName + ", Walkable: " + walkable);
       
            Tile tile = new Tile(imgName, walkable);
            
            fullTileArray[row][col] = tile;

            
            if(row < visibleBoardHeight && col < visibleBoardWidth){
                visibleTileArray[row][col] = tile;
            }

            col++;

            if(col >= fullTileArray[0].length){
            	col = 0;
            	row++;
            }
        }
    }

    public boolean canShift(int dir){
        if(dir == PokemonController.FACING_UP){
            if(charPos.getY() <= visibleBoardHeight / 2 && offsetY > 0){
                return true;
            }
        }else if(dir == PokemonController.FACING_RIGHT){
            if(charPos.getX() >= visibleBoardWidth / 2 && offsetX < fullTileArray[0].length - visibleBoardWidth){
                return true;
            }
        }else if(dir == PokemonController.FACING_DOWN){
            if(charPos.getY() >= visibleBoardHeight / 2 && offsetY < fullTileArray.length - visibleBoardHeight){
                return true;
            }
        }else if(dir == PokemonController.FACING_LEFT){
            if(charPos.getX() <= visibleBoardWidth / 2 && offsetX > 0){
                return true;
            }
        }

        return false;
    }

    public void shiftVisibleBoard(int dir){
        if(dir == PokemonController.FACING_UP){
            for(int j = 0; j < visibleBoardWidth; j++){
                for(int i = visibleBoardHeight - 1; i > 0; i--){
                    visibleTileArray[i][j] = visibleTileArray[i - 1][j];
                }
                visibleTileArray[0][j] = fullTileArray[offsetY - 1][j + offsetX];
            }
            offsetY--;
        }else if(dir == PokemonController.FACING_RIGHT){
            for(int i = 0; i < visibleBoardHeight; i++){
                for(int j = 0; j < visibleBoardWidth - 1; j++){
                    visibleTileArray[i][j] = visibleTileArray[i][j + 1];
                }
                visibleTileArray[i][visibleBoardWidth - 1] = fullTileArray[i + offsetY][visibleBoardWidth + offsetX]; //ADD OFFSET
            }
            offsetX++;
        }else if(dir == PokemonController.FACING_DOWN) {
            for(int j = 0; j < visibleBoardWidth; j++){
                for(int i = 0; i < visibleBoardHeight - 1; i++){
                    visibleTileArray[i][j] = visibleTileArray[i + 1][j];
                }
                visibleTileArray[visibleBoardHeight - 1][j] = fullTileArray[visibleBoardHeight + offsetY][j + offsetX];
            }
            offsetY++;
        }else if(dir == PokemonController.FACING_LEFT){
            for(int i = 0; i < visibleBoardHeight; i++){
                for(int j = visibleBoardWidth - 1; j > 0; j--){
                    visibleTileArray[i][j] = visibleTileArray[i][j - 1];
                }
                visibleTileArray[i][0] = fullTileArray[i + offsetY][offsetX - 1]; //ADD OFFSET
            }
            offsetX--;
        }
    }
    
    //not sure why this doesn't work- character can walk outside of the map
    public boolean validForWalking(int row, int col){
    	boolean valid = false;
    	
    	if( row < visibleBoardWidth && row >= 0 && col < visibleBoardHeight && col >= 0 ){
    		if( fullTileArray[row + offsetY][col + offsetX].isWalkable()){
    			valid = true;
    		}
    	}
       return valid;
    }

    public boolean canPokemonAppear(){
        return visibleTileArray[(int)charPos.getY()][(int)charPos.getX()].pokemonCanAppear();
    }

}

