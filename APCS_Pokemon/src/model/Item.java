package model;

//Class for Items

//itemName is the name of the item
//itemID is the ID of the item
public class Item{
	String itemName;
	int itemID;
	
	public Item(String name, int ID){
		this.itemName = name;
		this.itemID = ID;
	}
	
	
}